import { NgModule } from '@angular/core';
import {RouterModule, Routes} from "@angular/router";

import {BankexchangeComponent} from "./bankexchange/bankexchange.component";
import {BlogItemComponent} from "./blog/blog-item/blog-item.component";
import {NotFoundComponent} from "./not-found/not-found.component";
import {BlogListComponent} from "./blog/blog-list/blog-list.component";
import {CryptoComponent} from "./crypto/crypto.component";
import {NbuComponent} from "./nbu/nbu.component";
import {ConverterComponent} from "./converter/converter.component";

// определение маршрутов
const routes: Routes =[
    { path: '', component: BankexchangeComponent},
    { path: 'nbu', component: NbuComponent},
    { path: 'crypto', component: CryptoComponent},
    { path: 'converter', component: ConverterComponent},
    { path: 'news', component: BlogListComponent},
    { path: 'news/:slug', component: BlogItemComponent},
    { path: '**', component: NotFoundComponent }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ],
})
export class AppRoutingModule {}