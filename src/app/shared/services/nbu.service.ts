import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from "rxjs/Observable";

import {NbuCurrencyModel} from "../models/NbuCurrencyModel";

@Injectable()
export class NbuService {

  constructor(private http: HttpClient) { }

  getRates(url: string): Observable<NbuCurrencyModel[]>{
    return this.http.get<NbuCurrencyModel[]>(url);
  }

}
