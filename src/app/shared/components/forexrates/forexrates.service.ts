import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from "rxjs/Observable";
import {ForexModel} from "../../models/ForexModel";

@Injectable()
export class ForexratesService {

  constructor(private http: HttpClient) { }

  getRates(url: string): Observable<ForexModel>{
    return this.http.get<ForexModel>(url);
  }

}
