import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header-menu',
  templateUrl: './header-menu.component.html'
})
export class HeaderMenuComponent implements OnInit {

  menuItems = [
    {title: 'Курс валют', url: '/'},
    {title: 'Курс НБУ', url: '/nbu'},
    {title: 'Криптовалюты', url: '/crypto'},
    {title: 'Конвертер', url: '/converter'},
    {title: 'Новости', url: '/news'}
  ];

  constructor() { }

  ngOnInit() {
  }

}
