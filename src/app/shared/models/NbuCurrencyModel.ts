export class NbuCurrencyModel{
    constructor(
        public r030: number,
        public txt: string,
        public rate: number,
        public cc: string,
        public exchangedate: string
    ){}
}