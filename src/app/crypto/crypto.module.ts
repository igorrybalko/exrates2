import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CryptoComponent } from './crypto.component';
import {CryptoService} from "./crypto.service";

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [CryptoComponent],
  providers: [
      CryptoService
  ]
})
export class CryptoModule { }
