import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from "rxjs/Observable";

import {CryptoModel} from "../shared/models/CryptoModel";

@Injectable()
export class CryptoService {

  constructor(private http: HttpClient) { }

  getRates(url: string): Observable<CryptoModel[]>{
    return this.http.get<CryptoModel[]>(url);
  }

}
