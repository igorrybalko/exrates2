import { Component, OnInit } from '@angular/core';
import {CryptoService} from "./crypto.service";
import {CryptoModel} from "../shared/models/CryptoModel";

@Component({
  selector: 'app-crypto',
  templateUrl: './crypto.component.html'
})
export class CryptoComponent implements OnInit {

  cryptoCurrencies: CryptoModel[];

  filteredCryptoCurrencies: CryptoModel[];

  constructor(private cryptoService: CryptoService) { }

  ngOnInit() {

    this.cryptoService.getRates('/assets/cache/cripto.json').subscribe((response: CryptoModel[]) => {

      this.cryptoCurrencies = response;
      this.copyCryptoCurrencies();

    });

  }

  copyCryptoCurrencies(): void{
    this.filteredCryptoCurrencies = this.cryptoCurrencies.slice();
  }

  search(queryString: string): void{
    if(!queryString) this.copyCryptoCurrencies(); //when nothing has typed
    this.filteredCryptoCurrencies = this.cryptoCurrencies.slice().filter(
        item => item.name.toLowerCase().indexOf(queryString.toLowerCase()) > -1
    );
  }

}
