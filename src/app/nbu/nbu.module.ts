import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NbuComponent } from './nbu.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [NbuComponent],
})
export class NbuModule { }
