import { Component, OnInit } from '@angular/core';

import {NbuCurrencyModel} from "../shared/models/NbuCurrencyModel";
import {NbuService} from "../shared/services/nbu.service";


@Component({
  selector: 'app-nbu',
  templateUrl: './nbu.component.html'
})
export class NbuComponent implements OnInit {

  nbuCurrencies: NbuCurrencyModel[];

  filteredNbuCurrencies: NbuCurrencyModel[];

  constructor(private nbuService: NbuService) { }

  ngOnInit() {

    this.nbuService.getRates('/assets/cache/nbu.json').subscribe((response: NbuCurrencyModel[]) => {

      this.nbuCurrencies = response;
      this.copyNbuCurrencies();

    });
  }

  copyNbuCurrencies(): void{
    this.filteredNbuCurrencies = this.nbuCurrencies.slice();
  }

  search(queryString: string): void{
    if(!queryString) this.copyNbuCurrencies(); //when nothing has typed
    this.filteredNbuCurrencies = this.nbuCurrencies.slice().filter(
        item => item.txt.toLowerCase().indexOf(queryString.toLowerCase()) > -1
    );
  }

}
