import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import {NbuService} from "../shared/services/nbu.service";
import {NbuCurrencyModel} from "../shared/models/NbuCurrencyModel";

@Component({
  selector: 'app-converter',
  templateUrl: './converter.component.html'
})
export class ConverterComponent implements OnInit {

  nbuCurrencies: NbuCurrencyModel[];

  @ViewChild("selectGet") selectGet: ElementRef;
  @ViewChild("selectSet") selectSet: ElementRef;
  @ViewChild("inputSumm") inputSumm: ElementRef;

  result: number = 0;

  constructor(private nbuService: NbuService) { }

  ngOnInit() {

    this.nbuService.getRates('/assets/cache/nbu.json').subscribe((response: NbuCurrencyModel[]) => {

      this.nbuCurrencies = response;

    });
  }

  onChange(){
    let summ = +this.inputSumm.nativeElement.value,
        currSet = +this.selectSet.nativeElement.value,
        currGet = +this.selectGet.nativeElement.value;
    this.result = (currSet / currGet) * summ;

  }

}
