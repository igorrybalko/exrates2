import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { HttpClientModule }   from '@angular/common/http';

import { AppComponent } from './app.component';

import {AppRoutingModule} from "./app-routing.module";
import {BlogModule} from "./blog/blog.module";
import {BankexchangeModule} from "./bankexchange/bankexchangeModule";
import {NotFoundModule} from "./not-found/not-found.module";
import {HttpService} from "./shared/services/http.service";
import {FooterComponent} from "./shared/components/footer/footer.component";
import {HeaderComponent} from "./shared/components/header/header.component";
import {HeaderMenuComponent} from "./shared/components/header/header-menu/header-menu.component";
import {CryptoModule} from "./crypto/crypto.module";
import {NbuModule} from "./nbu/nbu.module";
import { ForexratesComponent } from './shared/components/forexrates/forexrates.component';
import {ConverterModule} from "./converter/converter.module";
import {NbuService} from "./shared/services/nbu.service";

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
    HeaderMenuComponent,
    ForexratesComponent
  ],
  providers: [
    HttpService,
    NbuService
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    BlogModule,
    BankexchangeModule,
    NotFoundModule,
    CryptoModule,
    NbuModule,
    ConverterModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
