import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {BlogListComponent} from "./blog-list/blog-list.component";
import {BlogItemComponent} from "./blog-item/blog-item.component";

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        BlogListComponent,
        BlogItemComponent
    ]
})
export class BlogModule { }