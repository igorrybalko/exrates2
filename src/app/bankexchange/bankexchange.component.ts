import { Component, OnInit, ViewChild, ElementRef  } from '@angular/core';
import {BankexchangeService} from "./bankexchange.service";

@Component({
  selector: 'app-bankexchange',
  templateUrl: './bankexchange.component.html',
})
export class BankexchangeComponent implements OnInit {

  banks: any = [{}];
  filteredBanks: any = [{}];
  date: any = '';
  @ViewChild("bankexTable") bankexTable: ElementRef;

  bankexShowCssClass = {
    usd: true,
    eur: false,
    rub: false
  };

  bankexSwitchEls: object[] = [
    {title: 'Доллар', id: 'usd'},
    {title: 'Евро', id: 'eur'},
    {title: 'Рубль', id: 'rub'}
  ];

  constructor(private bankexchangeService: BankexchangeService) { }

  ngOnInit(): void{

    this.bankexchangeService.getRates('/assets/cache/bankexchange.json').subscribe((response) => {

      this.banks = response.organizations;
      this.date = response.date;
      this.copyBanks();

    });

  }
  changeCurrency(id: string): void {

    switch (id){
      case 'eur':
        this.bankexShowCssClass.usd = false;
        this.bankexShowCssClass.eur = true;
        this.bankexShowCssClass.rub = false;
        break;
      case 'rub':
        this.bankexShowCssClass.usd = false;
        this.bankexShowCssClass.eur = false;
        this.bankexShowCssClass.rub = true;
        break;
      default:
        this.bankexShowCssClass.usd = true;
        this.bankexShowCssClass.eur = false;
        this.bankexShowCssClass.rub = false;
    }

  }

  copyBanks(): void{
    this.filteredBanks = this.banks.slice();
  }
  
  search(queryString: string): void{
    if(!queryString) this.copyBanks(); //when nothing has typed
    this.filteredBanks = this.banks.slice().filter(
        item => item.title.toLowerCase().indexOf(queryString.toLowerCase()) > -1
    )
  }

}
