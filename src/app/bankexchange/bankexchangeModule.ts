import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {BankexchangeComponent} from "./bankexchange.component";
import {BankexchangeService} from "./bankexchange.service";

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        BankexchangeComponent
    ],
    providers: [BankexchangeService]
})
export class BankexchangeModule { }