import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from "rxjs/Observable";

@Injectable()
export class BankexchangeService {

    constructor(private http: HttpClient) { }

    getRates(url: string): Observable<any>{
        return this.http.get(url);
    }
}
